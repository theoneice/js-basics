// these functions are called self-invoking functions
(function dataTypes() {
    // string, number, boolean, null, undefined
    const name = 'Ice Carev';
    const age = 33;
    const isMentor = true;
    const someProperty = null;
    const someUndefinedVar = undefined;
    let someOtherProperty; // default is always undefined! 
    // Only let variable can be uninitialized!

    console.log(name);
    console.log(age);
    console.log(isMentor);
    console.log(someProperty);
    console.log(someOtherProperty);
    console.log(typeof isMentor);
    console.log(typeof someOtherProperty);
})();


(function stringConcationation() {
    const name = 'Ice Carev';
    const age = 33;

    // concatinate with a +
    console.log('my name is ' + name + ', i am ' + age + ' years old!');

    // concatinate with string templates!
    console.log(`my name is ${name}, i am ${age} years old!`);

})();

(function stringMethods() {
    const name = 'Ice Carev';

    console.log(name.length);
    console.log(name.substring(0, 4));
    console.log(name.indexOf('Ice')); // index of the first character, when it is found
    console.log(name.indexOf('bla')); // -1 when it is not found
    console.log(name.toLowerCase());
    console.log(name.toUpperCase());
    console.log(name.split(' '));
    console.log('Ice Carev'.includes('Care'));
})();

(function arrays() {
    const names = ['Ice Carev', 'Zorica Careva', 'Viktor Jovanovski']; // array of strigs
    const typesArray = [
        'Ice Carev', 22, true, null, {firstName: 'Ice', lastName: 'Carev'}
    ];
    const matrixArray = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15, 16]
    ]; // Array 4x4
    names[3] = 'Other guy'; // Zero based index
    names.push('yet another guy');

    // Arrays in JS are really just maps that can behave like an array or a list
    const hashTable = [];
    hashTable['apple'] = 'green';
    hashTable['pineapple'] = 'yellow';
    hashTable['vegetables'] = [];
    hashTable['vegetables'].push('cabagge');
    hashTable['vegetables'].push('cucumber');
    hashTable['vegetables'].push('tomato');
    console.log(hashTable);

    console.log(names); // Printing an array is easy
    console.log(typesArray);
    console.log(matrixArray);

    const deletedItem = names.pop(4); // delete item on index 4
    console.log(deletedItem);
    console.log(names); // last item deleted
    console.log(names.slice(0, 2)); // returns a sub-set of the array
    names.splice(3, 1); // delete the item on index 3
    console.log(names);
    
    const foundName = names.find(item => item === 'Ice Carev'); // Finds one item or null
    console.log(foundName);

    names.unshift('First girl'); // Adds to the beginning of the array
    console.log(names);

    // Returns an arrays always
    const foundArray = names.filter(item => item === 'Ice Carev'); 
    console.log(foundArray);

    console.log(names.includes('Ice Carev')); // looks for an item with the exact value

    const arrayWithConstructor = new Array('one', 'two', 'three'); // SHOULD NOT BE USED!!!
    console.log(arrayWithConstructor);

    // Returns object becouse of how arrays can be treated as objects
    console.log(typeof typesArray);
    console.log(Array.isArray(typesArray));
    
})();

(function objects() {
    const person = {
        firstName: 'Ice',
        lastName: 'Carev',
        age: 33,
        isMentor: true,
        hobbies: ['reading', 'photography', 'writing short stories'],
        address: {
            street: 'Prins Frederikplein',
            number: 439,
            postalCode: '3071KS',
            city: 'Rotterdam',
            country: 'Netherlands'
        }
    };

    console.log(person);
    console.log(person.address.city);
    console.log(person.hobbies[1]);

    const { firstName, age, address: { city } } = person; // this is called destructuring
    /* 
        Destructuring is a way to 'deconstruct' a JS object and get only the properties
        as separate variables
    */
    console.log(firstName);
    console.log(age);
    console.log(city);

    person.email = 'theoneice@gmail.com'; // We can dynamically add properties to an object
    console.log(person.email);
    console.log(person);

    const todos = [
        {
            id: 1,
            text: 'Todo 1',
            isCompleted: true
        },
        {
            id: 2,
            text: 'Todo 2',
            isCompleted: false
        },
        {
            id: 3,
            text: 'Todo 3',
            isCompleted: false
        },
        {
            id: 4,
            text: 'Todo 4',
            isCompleted: true
        }
    ];

    console.log(todos);
    console.log(todos[1].text); // print the second todo text

    // This is a stringified JS object, this is what is sent to the server
    const personJSON = JSON.stringify(person);
    console.log(personJSON);

    // This is a stringified JS array, this is what is sent to the server
    const todosJSON = JSON.stringify(todos);
    console.log(todosJSON);

    // This is a parsed JSON object back to JS object, this is what we get from the server
    const personBackToObject = JSON.parse(personJSON);
    console.log(personBackToObject);

    // This is a parsed JSON array back to JS array, this is what we get from the server
    const todosBackToArray = JSON.parse(todosJSON);
    console.log(todosBackToArray);
})();


(function forWhileAdvancedFor() {
    const todos = [
        {
            id: 1,
            text: 'Todo 1',
            isCompleted: true
        },
        {
            id: 2,
            text: 'Todo 2',
            isCompleted: false
        },
        {
            id: 3,
            text: 'Todo 3',
            isCompleted: false
        },
        {
            id: 4,
            text: 'Todo 4',
            isCompleted: true
        }
    ];

    // For
    for (let i = 0; i < 10; i++) {
        console.log(`Number: ${i}`);
    }

    // For of loop
    for (let todo of todos) {
        console.log(todo.text);
    }

    // While
    let i = 0;
    while (i < 10) {
        console.log(`Number: ${i}`);
        i++;
    }
})();


(function foreachMapFilter() {
    const todos = [
        {
            id: 1,
            text: 'Todo 1',
            isCompleted: true
        },
        {
            id: 2,
            text: 'Todo 2',
            isCompleted: false
        },
        {
            id: 3,
            text: 'Todo 3',
            isCompleted: false
        },
        {
            id: 4,
            text: 'Todo 4',
            isCompleted: true
        }
    ];

    // Foreach
    todos.forEach(item => console.log(item.text));

    // Same thing but with using map()
    const todoTexts = todos.map(item => item.text);
    console.log(todoTexts);

    // Filter only the completed todos
    const completed = todos.filter(item => item.isCompleted);
    console.log(completed);

    // This is the same as the above, only with a normal function
    const completedWithFunction = todos.filter(function(item) {
        return item.isCompleted;
    });
    console.log(completedWithFunction);

    // TODO check what is the correct usage!!!
    const concatenatedTodos = todos.map(item => item.text)
        .reduce((item1, item2) => `${item1.text}, ${item2.text}`);
    console.log(concatenatedTodos);
})();


(function conditionals() {
    const xAsString = '10';

    // With "==" we just compare the values
    if (xAsString == 10) {
        console.log('Yey!');
    } else {
        console.log('Awww!');  
    }

    const xAsNumber = 10;

    // With "===" we compare the values and the types
    // Use this way!!!
    if (xAsNumber === 10) {
        console.log('Yey!');
    } else {
        console.log('Awww!');  
    }

    // Ternary operator
    console.log(xAsNumber === 10 ? 'Yey!' : 'Awww!');

    // Switch
    const color = 'blue';
    switch(color) {
        case 'blue': {
            console.log('blue'); 
            break;
        }
        case 'red':
            console.log('red'); 
            break;
        default:
            break;
    }
})();


(function functions() {
    function addWithoutDefaultValues(num1, num2) {
        return num1 + num2;
    }

    console.log(addWithoutDefaultValues()); // By not passing param values, we get a NAN (not a number) result

    function addWithDefaultValues(num1 = 1, num2 = 1) {
        return num1 + num2;
    }
    console.log(addWithDefaultValues()); // With default values
    console.log(addWithDefaultValues(5, 10));
    console.log(addWithDefaultValues(10));

    // addWithDefaultValues() as an arrow function/lambda
    const addAsArrowFunc = (num1 = 1, num2 = 1) => num1 + num2;
    console.log(addAsArrowFunc(5, 10));

    function opeartionOnNumbers(num1, num2, operationFn) {
        return operationFn(num1, num2);
    }

    console.log(opeartionOnNumbers(5, 10, addAsArrowFunc)); // variable function
    console.log(opeartionOnNumbers(5, 10, (num1 = 1, num2 = 1) => num1 + num2)); // lambda
    console.log(opeartionOnNumbers(5, 10, function(num1, num2) {
        return num1 + num2;
    })); // Anonymous function

    console.log(5, 10, 'ice', true); // print multiple values at the same time

})();


(function OOPInJs() {
    // Constructor function
    function Person(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);

        // this.getBirthYear = function() {
        //     return this.dob.getFullYear();
        // };

        this.getFullName = () => `${this.firstName} ${this.lastName}`;
    }

    // Prototipical extension
    Person.prototype.getBirthYear = function() {
        return this.dob.getFullYear();
    };

    // Instatiating object
    const ice = new Person('Ice', 'Carev', '11-11-1987');
    const zorica = new Person('Zorica', 'Careva', '06-01-1992');
    console.log(ice);
    console.log(ice.getBirthYear());
    console.log(ice.getFullName());
    console.log(zorica);
    console.log(zorica.firstName);

    // ES6/ES2015 classes
    // PREFER THIS METHOD !!!
    class PersonClass {
        constructor(firstName, lastName, dob) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.dob = new Date(dob);
        }

        getFullName() {
            return `${this.firstName} ${this.lastName}`
        }

        getBirthYear() {
            return this.dob.getFullYear();
        }
    }

    const antoherPerson = new PersonClass('Another', 'Person', '10-02-1985');
    console.log(antoherPerson);
    console.log(antoherPerson.getBirthYear());
})();


(function selectingWithTheDOM() {
    // implicit DOM objects
    console.log(window);
    window.console.log('console log from window');
    console.log(document);

    // Single element selector
    const form = document.getElementById('my-form');
    console.log(form);

    const formSelected = document.querySelector('#my-form');
    console.log(formSelected);

    const containerSelected = document.querySelector('.container');
    console.log(containerSelected);

    const h1Selected = document.querySelector('h1');
    console.log(h1Selected);

    const elementFromComplexSelector = document.querySelector('.container #users');
    console.log(elementFromComplexSelector);
    
    // Multiple elements selector
    const allDivsSelected = document.querySelectorAll('div');
    console.log(allDivsSelected);

    allDivsSelected.forEach(item => console.log(item.textContent));
})();


(function manipulatingTheDOM() {
    const itemsUl = document.querySelector('.items');
    // items.remove(); // Remove the whole <ul class="items"> element
    
    // set text of the first element
    itemsUl.firstElementChild.textContent = 'Hello World!';

    // Set the text for the 2-nd <li>
    itemsUl.children[1].innerText = 'Ice';

    // Add HTML to an element
    itemsUl.lastElementChild.innerHTML = '<h1>Hey there!</h1>';

    const submitBtn = document.querySelector('input.btn');
    submitBtn.style.background = 'red';
    // submitBtn.className = 'no-class';

})();


(function events() {
    const submitBtn = document.querySelector('input.btn');

    const btnClickEvent = (e) => {
        e.preventDefault(); // this prevents the submit functionality
        console.log('clicked');
        console.log(e);
        console.log(e.target);
        console.log(e.target.className);
        // document.querySelector('#my-form').style.background = '#ccc';
        // document.querySelector('body').classList.add('bg-dark');
        document.querySelector('.items').firstElementChild.innerHTML = '<h1>Hello</h1>';
    };

    // Here we are hadling the click event
    submitBtn.addEventListener('click', btnClickEvent);

    // submitBtn.removeEventListener('click', btnClickEvent, true);

    submitBtn.addEventListener('mouseover', (e) => {
        document.querySelector('.items').lastElementChild.innerHTML = '<h1>Hello</h1>';
    });

    submitBtn.addEventListener('mouseout', (e) => {
        document.querySelector('.items').lastElementChild.innerHTML = '<h1>bye</h1>';
    });

})();


// (function formHandler() {
//     const form = document.querySelector('#my-form');
//     const nameInput = document.querySelector('#name');
//     const emailInput = document.querySelector('#email');
//     const submitBtn = document.querySelector('.btn');
//     const msg = document.querySelector('.msg');
//     const users = document.querySelector('#users');

//     form.addEventListener('submit', onSubmit);

//     function onSubmit(e) {
//         e.preventDefault();

//         if (nameInput.value === '' || emailInput.value === '') {
//             msg.classList.add('error');
//             msg.innerHTML = 'Name and Email are required!';
//         } else {
//             msg.innerHTML = 'Success!';
//         }
//     }
// })();